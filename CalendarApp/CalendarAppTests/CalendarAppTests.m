//
//  CalendarAppTests.m
//  CalendarAppTests
//
//  Created by Pramod Jadhav on 07/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MonthItem.h"
#import "NSDate+DatesAddition.h"

@interface CalendarAppTests : XCTestCase

@end

@implementation CalendarAppTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testObjectCreation {
    
    MonthItem * month = [MonthItem currentMonth];
    XCTAssertTrue(month!=nil, @"Failed to get current month item");
    
    NSArray * monthItems = [MonthItem numbedOfMonths:2 afterDate:[NSDate date] ];
    XCTAssertTrue([monthItems count]==2, @"Failed to get months after given date");

    NSArray * days = month.allDates;
    XCTAssertTrue([days count]>0, @"Failed to get dates of month");

}

- (void)testDateComparison {
    
    NSArray * monthItems = [MonthItem numbedOfMonths:2 afterDate:[NSDate date] ];
    MonthItem * month = [monthItems firstObject];
    DayItem * calItem = [month.allDates firstObject];
    XCTAssertTrue([calItem.date compare:[NSDate date]]==NSOrderedDescending, @"Dates in next month are wrong");
    
    monthItems = [MonthItem numbedOfMonths:2 beforeDate:[NSDate date] ];
    month = [monthItems firstObject];
    calItem = [month.allDates firstObject];
    XCTAssertTrue([calItem.date compare:[NSDate date]]==NSOrderedAscending, @"Dates in before month are wrong");

}

- (void)testMonthListPerformance {
    // This is an example of a performance test case.
    [self measureBlock:^{
        
        NSArray * monthItems = [MonthItem numbedOfMonths:12 afterDate:[NSDate date] ];
    }];
}

- (void)testEventFetchPerformance {
    // This is an example of a performance test case.
    [self measureBlock:^{
        
        MonthItem * month = [MonthItem currentMonth];
        DayItem * calItem = [month.allDates firstObject];
        [calItem events];
        
    }];
}
@end
