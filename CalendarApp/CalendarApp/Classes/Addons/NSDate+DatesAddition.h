//
//  UIView+DatesOfMonth.h
//  CalendarApp
//
//  Created by Pramod Jadhav on 08/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSDate (DatesAddition)

/*
 Returns all NSDates in given month
 */
-(NSArray *)da_alldatesInMonth;

/*
  Returns day of the month as numeric string & if day if first day of month it returns name of the month along with day
 */
-(NSString *)da_dayString;

/*
 Compares date with receiver , compares only Day,month & year part of both dates
 */
-(BOOL )da_equalsToDate:(NSDate *)date;

/*
 Returnes NSDate with having beginning of the day
 */
-(NSDate *)da_beginningOfDay:(NSDate *)date;

/*
 Returnes NSDate with having end of the day
 */
-(NSDate *)da_endOfDay:(NSDate *)date;

/*
 Returnes NSDates having offset (it can be +ve or -ve) as per given offset as month
 */
-(NSDate *)da_monthWithOffset:(NSInteger)offset;

@end
