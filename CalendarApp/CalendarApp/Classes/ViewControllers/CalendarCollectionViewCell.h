//
//  CalendarCollectionViewCell.h
//  CalendarApp
//
//  Created by Pramod Jadhav on 08/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DayItem.h"

@interface CalendarCollectionViewCell : UICollectionViewCell

- (void)configure:(DayItem *)calendarItem ;
    
@end
