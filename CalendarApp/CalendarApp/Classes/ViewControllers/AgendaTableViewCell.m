//
//  AgendaTableViewCell.m
//  CalendarApp
//
//  Created by Pramod Jadhav on 08/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//

#import "AgendaTableViewCell.h"

@implementation AgendaTableViewCell
- (void)configure:(EventItem  *)event{
    
    self.textLabel.text = event.title;
}
@end
