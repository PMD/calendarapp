//
//  CalendarController.m
//  CalendarApp
//
//  Created by Pramod Jadhav on 08/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//

#import "CalendarController.h"
#import "CalendarView.h"
#import "AgendaView.h"
#import <EventKit/EventKit.h>

@interface CalendarController () <CalendarViewDelegate, AgendaViewDelegate>
@property (nonatomic) IBOutlet CalendarView * calendarView;
@property (nonatomic) IBOutlet AgendaView * agendaView;
@end

@implementation CalendarController

-(void)dealloc{

    self.calendarView = nil;
    self.agendaView = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.calendarView.delegate = self;
    self.agendaView.delegate = self;
    UIBarButtonItem * todayButton = [[UIBarButtonItem alloc] initWithTitle:@"Today" style:UIBarButtonItemStylePlain target:self action:@selector(todayButtonActon:)];
    self.navigationItem.rightBarButtonItem = todayButton;
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    if([EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent] == EKAuthorizationStatusAuthorized){
        
        [self loadViewData];
        
    }else{
        
        EKEventStore *store = [[EKEventStore alloc] init];
        
        // Request access for device calendar
        [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            if(!granted) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController * controller = [UIAlertController alertControllerWithTitle:@"Message" message:@"Calendar access is not given, app will not be able to get events from calendar in device" preferredStyle:UIAlertControllerStyleActionSheet];
                    UIAlertAction * action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        
                        [controller dismissViewControllerAnimated:YES completion:^{
                            
                        }];
                    }];
                    
                    [controller addAction:action];
                    [self  presentViewController:controller animated:YES completion:^{
                        
                    }];
                });
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self loadViewData];
            });
            
        }];
    }
    
}

-(void)loadViewData{
    
    NSArray * array = [MonthItem numbedOfMonths:4 beforeDate:[NSDate date] ];
    NSMutableArray * agendaVData = [NSMutableArray new];
    NSMutableArray * calendarVData = [NSMutableArray new];
    [calendarVData addObjectsFromArray:array];
    for (MonthItem * month in array) {
        
        [agendaVData addObjectsFromArray:month.allDates];
    }
    
    MonthItem * currentMonth = [MonthItem currentMonth];
    [agendaVData addObjectsFromArray:currentMonth.allDates];
    [calendarVData addObject:currentMonth];
    
    array = [MonthItem numbedOfMonths:8 afterDate:[NSDate date] ];
    [calendarVData addObjectsFromArray:array];
    for (MonthItem * month in array) {
        
        [agendaVData addObjectsFromArray:month.allDates];
    }
    [self.calendarView load:calendarVData];
    [self.agendaView load:agendaVData];
    
}

-(void)todayButtonActon:(UIBarButtonItem *)sender{
    
    [self.calendarView selectToday];
}

#pragma mark- CalendarViewDelgate methods
-(void)didSelectItem:(DayItem *)calendarItem{
    
    self.agendaView.shouldActOnScroll = NO;
    [self.agendaView loadEventsForItem:calendarItem];
}

-(void)didScrolledToMonth:(MonthItem *)monthItem{
    
    self.navigationItem.title = monthItem.monthString;
}

#pragma mark- AgendaViewDelgate methods
-(void)didScrollToCalndarItem:(DayItem *)calItem{
    
    [self.calendarView selectCalendarItem:calItem];
}

@end
