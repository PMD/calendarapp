//
//  CalendarView.h
//  CalendarApp
//
//  Created by Pramod Jadhav on 08/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MonthItem.h"

@protocol CalendarViewDelegate <NSObject>

-(void)didSelectItem:(DayItem *)calendarItem;
-(void)didScrolledToMonth:(MonthItem *)monthItem;

@end

@interface CalendarView : UIView
@property (nonatomic, assign) id<CalendarViewDelegate> delegate;

/*
 Select calendarItem in Calendar view
 */
-(void)selectCalendarItem:(DayItem *)calendarItem;

/*
 Select calendarItem having today's date
 */
-(void)selectToday;


/*
 Loads Months from dataArray in Calendar view
 */
-(void)load:(NSArray *)dataArray;
@end
