//
//  CalendarCollectionViewCell.m
//  CalendarApp
//
//  Created by Pramod Jadhav on 08/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//

#import "CalendarCollectionViewCell.h"

@interface CalendarCollectionViewCell()
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIView *eventIndicatorView;

@end

@implementation CalendarCollectionViewCell

-(void)awakeFromNib{
    
    [super awakeFromNib];
    self.dateLabel.layer.cornerRadius = 20.0f;
    self.dateLabel.layer.masksToBounds = YES;
    self.dateLabel.numberOfLines = 0;
}

- (void)configure:(DayItem *)calendarItem {
    
    self.dateLabel.text = [calendarItem.date da_dayString];
    if(calendarItem.isSelected){
        
        self.dateLabel.backgroundColor = [UIColor lightGrayColor];
    }else{
        
        self.dateLabel.backgroundColor = [UIColor clearColor];
    }
    self.eventIndicatorView.hidden = !calendarItem.haveEvents;
}

@end
