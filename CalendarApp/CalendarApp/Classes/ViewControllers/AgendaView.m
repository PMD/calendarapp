//
//  AgendaView.m
//  CalendarApp
//
//  Created by Pramod Jadhav on 08/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//

#import "AgendaView.h"
#import "AgendaTableViewCell.h"
#import "MonthItem.h"

@interface AgendaView()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,weak) IBOutlet UITableView * eventsTableView;
@property (nonatomic,strong) NSArray * dataArray;
@property (nonatomic,strong) NSDateFormatter * dateFormatter;
@property (nonatomic,assign) NSInteger prevSection;
@end

@implementation AgendaView

-(void)dealloc{
    
    self.dataArray = nil;
    self.dateFormatter = nil;
}

-(void)awakeFromNib{
    
    [super awakeFromNib];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateStyle = NSDateFormatterFullStyle;
    self.dateFormatter.timeStyle = NSDateFormatterNoStyle;
}

-(NSIndexPath *)indexPathforItem:(DayItem *)calItem{
    
    __block NSIndexPath * indexPath = nil;
    [self.dataArray enumerateObjectsUsingBlock:^(DayItem  * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
       
        if([obj.date da_equalsToDate:calItem.date]){
            
            indexPath = [NSIndexPath indexPathForRow:0 inSection:idx];
            *stop = YES;
        }
    }];
    return indexPath;
}

-(void)loadEventsForItem:(DayItem *)calItem{
    
    [self.eventsTableView scrollToRowAtIndexPath:[self indexPathforItem:calItem] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

#pragma mark - Table view delegate methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    DayItem * item = [self.dataArray objectAtIndex:section];
    return [item.events count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return [self.dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AgendaTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"AgendaTableViewCell" forIndexPath:indexPath];
    DayItem * item = [self.dataArray objectAtIndex:indexPath.section];
    [cell configure:item.events[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 44;
}

-(void)load:(NSArray *)dataArray{
    
    self.dataArray = dataArray;
    [self.eventsTableView reloadData];
    DayItem * calItem = [[DayItem alloc] initWithDate:[NSDate date] isSelected:NO];
    [self.eventsTableView scrollToRowAtIndexPath:[self indexPathforItem:calItem] atScrollPosition:UITableViewScrollPositionTop animated:YES];    
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    DayItem * item = [self.dataArray objectAtIndex:section];
    return [self.dateFormatter stringFromDate:item.date];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 30;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if(self.shouldActOnScroll){
        NSUInteger sectionNumber = [[self.eventsTableView indexPathForCell:[[self.eventsTableView visibleCells] objectAtIndex:0]] section];
        if(self.prevSection != sectionNumber){
            DayItem * item = [self.dataArray objectAtIndex:sectionNumber];
            [self.delegate didScrollToCalndarItem:item];
        }
        self.prevSection = sectionNumber;
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    self.shouldActOnScroll = YES;
    return [super hitTest:point withEvent:event];
}

@end
