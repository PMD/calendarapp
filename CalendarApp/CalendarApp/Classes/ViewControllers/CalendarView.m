//
//  CalendarView.m
//  CalendarApp
//
//  Created by Pramod Jadhav on 08/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//



#import "CalendarView.h"
#import "CalendarCollectionViewCell.h"

static NSString * const reuseIdentifier = @"CalendarCollectionViewCell";

@interface CalendarView () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (nonatomic) IBOutlet UICollectionView * collectionView;
@property (nonatomic) IBOutlet UIView * daysLabelView;
@property (nonatomic) NSArray * dataArray;
@property (nonatomic,strong) NSIndexPath * prevIndexPath;
@end

@implementation CalendarView

#pragma mark - UICollectionViewDataSource

-(void)dealloc{
    
    self.dataArray = nil;
    self.collectionView.delegate = nil;
    self.collectionView.dataSource = nil;
    self.collectionView = nil;
    self.prevIndexPath = nil;
}

-(void)didSelectIndexpath:(NSIndexPath *)indexPath{
    
    MonthItem * month = self.dataArray[indexPath.section];
    DayItem * calendarItem = [month.datesWithPadding objectAtIndex:indexPath.row];
    if (!calendarItem.date) {
        return;
    }
    if(self.prevIndexPath){
        
        MonthItem * month = self.dataArray[self.prevIndexPath.section];
        DayItem * calendarItem = [month.datesWithPadding objectAtIndex:self.prevIndexPath.row];
        calendarItem.isSelected = NO;
        
    }
    month = self.dataArray[indexPath.section];
    calendarItem = [month.datesWithPadding objectAtIndex:indexPath.row];
    calendarItem.isSelected = YES;
    if(self.prevIndexPath){
        [self.collectionView reloadItemsAtIndexPaths:@[self.prevIndexPath,indexPath]];
        
    }else{
        [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
    }
    self.prevIndexPath = indexPath;
}

-(void)selectToday{
    
    DayItem * calItem = [[DayItem alloc] initWithDate:[NSDate date] isSelected:NO];
    [self selectCalendarItem:calItem];

}

-(void)selectCalendarItem:(DayItem *)calendarItem{
    
    __block NSInteger section = -1;
    __block NSInteger row = -1;
    __block MonthItem * month = nil;
    [self.dataArray enumerateObjectsUsingBlock:^(MonthItem *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        
        row = [obj indexOfItemsWithPadding:calendarItem];
        if(row != -1){
            
            month = obj;
            section = idx;
            *stop = YES;

        }
    }];
    if (section>-1 && row>-1) {
        
        NSIndexPath * indexPath = [NSIndexPath indexPathForItem:row inSection:section];
        [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
        [self didSelectIndexpath:indexPath];
        [self.delegate didScrolledToMonth:month];

    }
}

-(void)awakeFromNib{
    
    [super awakeFromNib];
    self.daysLabelView.hidden = YES;
    self.dataArray = [ NSMutableArray array];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CalendarSectionHeader" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CalendarSectionHeader"];
    
    NSArray * days = @[@"M",@"T",@"W",@"T",@"F",@"S",@"S"];
    CGFloat start = 0,width = CGRectGetWidth([UIScreen mainScreen].bounds)/7.0;
    for (NSInteger index = 0; index < [days count]; index++) {
        
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(start, 0, width, 20)];
        [label setFont:[UIFont systemFontOfSize:14.0f]];
        label.contentMode = UIViewContentModeCenter;
        label.textAlignment = NSTextAlignmentCenter;
        [self.daysLabelView addSubview:label];
        [label setText:days[index]];
        start += width;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if([self.prevIndexPath isEqual:indexPath]) {
        return ;
    }
    [self didSelectIndexpath:indexPath];
    
    MonthItem * month = self.dataArray[indexPath.section];
    DayItem * calendarItem = [month.datesWithPadding objectAtIndex:indexPath.row];
    [self.delegate didSelectItem:calendarItem];
    self.prevIndexPath = indexPath;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.dataArray count];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    MonthItem * month = self.dataArray[section];
    return [month.datesWithPadding count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CalendarCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    MonthItem * month = self.dataArray[indexPath.section];
    DayItem * calendarItem = [month.datesWithPadding objectAtIndex:indexPath.row];
    [cell configure:calendarItem];
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return  CGSizeMake((self.bounds.size.width-60)/7, (-60+self.bounds.size.width)/7);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsZero;
}

-(void)load:(NSArray *)dataArray{
    
    self.dataArray = dataArray;
    [self.collectionView reloadData];
    [self selectToday];
    self.daysLabelView.hidden = NO;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    NSUInteger sectionNumber = [[self.collectionView indexPathForCell:[[self.collectionView visibleCells] firstObject]] section];
    if(sectionNumber < [self.dataArray count]){
        MonthItem * month = self.dataArray[sectionNumber];
        [self.delegate didScrolledToMonth:month];
    }
}
#pragma mark -

@end
