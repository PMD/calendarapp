//
//  AgendaView.h
//  CalendarApp
//
//  Created by Pramod Jadhav on 08/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DayItem.h"

@protocol AgendaViewDelegate <NSObject>

- (void)didScrollToCalndarItem:(DayItem *)calItem;

@end

@interface AgendaView : UIView
@property (nonatomic, assign) id<AgendaViewDelegate>delegate;
@property (nonatomic, assign) BOOL shouldActOnScroll;

/*
 Scrolls to events of provided calendarItem in Agenda view
 */
-(void)loadEventsForItem:(DayItem *)calItem;

/*
 Loads events for given array for CalendarItem in Agenda view
 */
-(void)load:(NSArray *)dataArray;
@end
