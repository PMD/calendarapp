//
//  CalendarItem.h
//  CalendarApp
//
//  Created by Pramod Jadhav on 08/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSDate+DatesAddition.h"

@interface DayItem : NSObject

- (DayItem *)initWithDate:(NSDate *)date isSelected:(BOOL)selected;


@property (nonatomic,readonly) NSArray * events;
@property (nonatomic,readonly) NSDate * date;
@property (nonatomic) BOOL isSelected;
@property (nonatomic,readonly) BOOL haveEvents;
@end
