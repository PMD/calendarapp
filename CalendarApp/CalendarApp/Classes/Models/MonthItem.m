//
//  MonthItem.m
//  CalendarApp
//
//  Created by Pramod Jadhav on 08/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//

#import "MonthItem.h"

@implementation MonthItem

- (MonthItem *)initWithDate:(NSDate *)date{
    
    self = [super init];
    if (self) {
        
        _date = date;
        NSMutableArray * datesThisMonth = [NSMutableArray new];
        
        /*
         NSCalendar *cal = [NSCalendar currentCalendar];
         
         NSDate * d = [[_date da_alldatesInMonth] firstObject];
         NSDateComponents *components = [cal components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitWeekday | NSCalendarUnitEra) fromDate:d];
         
         NSInteger end = components.weekday-1;
         if(end == 0){
         end = 7;
         }
         
         for (NSInteger index = 1; index<(end); ++index) {
         
         CalendarItem * calendarItem = [[CalendarItem alloc] initWithDate:nil isSelected:NO];
         [datesThisMonth addObject:calendarItem];
         
         }
         */
        
        for (NSDate * date in [_date da_alldatesInMonth]) {
            
            DayItem * calendarItem = [[DayItem alloc] initWithDate:date isSelected:NO];
            
            if([date da_equalsToDate:[NSDate date]]){
                
                calendarItem.isSelected = YES;
            }
            [datesThisMonth addObject:calendarItem];
            
        }
        _allDates = [NSArray arrayWithArray:datesThisMonth];
    }
    return self;
}

- (NSArray *)datesWithPadding{
    
    NSMutableArray * datesThisMonth = [NSMutableArray new];
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    NSDate * d = [[_date da_alldatesInMonth] firstObject];
    NSDateComponents *components = [cal components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitWeekday | NSCalendarUnitEra) fromDate:d];
    
    NSInteger end = components.weekday-1;
    if(end == 0){
        end = 7;
    }
    
    for (NSInteger index = 1; index<(end); ++index) {
        
        DayItem * calendarItem = [[DayItem alloc] initWithDate:nil isSelected:NO];
        [datesThisMonth addObject:calendarItem];
        
    }
    [datesThisMonth addObjectsFromArray:_allDates];

    return  [NSArray arrayWithArray:datesThisMonth];
}

- (MonthItem *)initWithDate:(NSDate *)date withPadding:(BOOL)shouldAd{
    
    self = [super init];
    if (self) {
        
        _date = date;
        
        NSMutableArray * datesThisMonth = [NSMutableArray new];
        /*
        if(shouldAd){
            NSCalendar *cal = [NSCalendar currentCalendar];
            
            NSDate * d = [[_date da_alldatesInMonth] firstObject];
            NSDateComponents *components = [cal components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitWeekday | NSCalendarUnitEra) fromDate:d];
            
            NSInteger end = components.weekday-1;
            if(end == 0){
                end = 7;
            }
            
            for (NSInteger index = 1; index<(end); ++index) {
                
                CalendarItem * calendarItem = [[CalendarItem alloc] initWithDate:nil isSelected:NO];
                [datesThisMonth addObject:calendarItem];
                
            }
        }
        */
        
        for (NSDate * date in [_date da_alldatesInMonth]) {
            
            DayItem * calendarItem = [[DayItem alloc] initWithDate:date isSelected:NO];
            
            if([date da_equalsToDate:[NSDate date]]){
                
                calendarItem.isSelected = YES;
            }
            [datesThisMonth addObject:calendarItem];
            
        }
        _allDates = [NSArray arrayWithArray:datesThisMonth];
    }
    return self;
}


-(NSInteger)indexOfItem:(DayItem *)calItem{
    
    __block NSInteger index = -1;
    [self.allDates enumerateObjectsUsingBlock:^(DayItem *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if([obj.date da_equalsToDate:calItem.date]){
            
            index = idx;
            *stop = YES;
        }
    }];
    return index;
}

-(NSInteger)indexOfItemsWithPadding:(DayItem *)calItem{
    
    __block NSInteger index = -1;
    [self.datesWithPadding enumerateObjectsUsingBlock:^(DayItem *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if([obj.date da_equalsToDate:calItem.date]){
            
            index = idx;
            *stop = YES;
        }
    }];
    return index;
}


-(NSString *)monthString{
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM yyyy"];
    return [formatter stringFromDate:self.date];
    
}

+ (NSMutableArray * )numbedOfMonths:(NSInteger)number beforeDate:(NSDate *)date{
    
    NSMutableArray *array = [NSMutableArray array];
    for (NSInteger i = 0; i<number; i++) {
        
        NSDate * newDate = [date da_monthWithOffset:-((number-i))];
        MonthItem * month = [[MonthItem alloc] initWithDate:newDate];
        [array addObject:month];
    }
    return array;
}

+ (NSMutableArray * )numbedOfMonths:(NSInteger)number afterDate:(NSDate *)date{
    
    NSMutableArray *array = [NSMutableArray array];
    for (NSInteger i = 1; i<=number; i++) {
        
        NSDate * newDate = [date da_monthWithOffset:i];
        MonthItem * month = [[MonthItem alloc] initWithDate:newDate];
        [array addObject:month];
        
    }
    return array;
}

+ (MonthItem * )currentMonth{

    MonthItem * month = [[MonthItem alloc] initWithDate:[NSDate date] withPadding:NO];
    return month;
}

@end
