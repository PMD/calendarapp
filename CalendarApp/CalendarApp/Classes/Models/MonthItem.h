//
//  MonthItem.h
//  CalendarApp
//
//  Created by Pramod Jadhav on 08/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DayItem.h"

@interface MonthItem : NSObject

- (MonthItem *)initWithDate:(NSDate *)date;
+ (NSMutableArray * )numbedOfMonths:(NSInteger)number beforeDate:(NSDate *)date;
+ (NSMutableArray * )numbedOfMonths:(NSInteger)number afterDate:(NSDate *)date;

@property (nonatomic,readonly) NSDate * date;
@property (nonatomic,readonly) NSArray * allDates;

- (NSString *)monthString;

-(NSInteger)indexOfItem:(DayItem *)calItem;
-(NSInteger)indexOfItemsWithPadding:(DayItem *)calItem;

- (MonthItem *)initWithDate:(NSDate *)date withPadding:(BOOL)shouldAd;

- (NSArray *)datesWithPadding;

+ (MonthItem * )currentMonth;
@end
