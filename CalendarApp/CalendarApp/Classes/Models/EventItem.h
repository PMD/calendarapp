//
//  EventItem.h
//  CalendarApp
//
//  Created by Pramod Jadhav on 08/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EventKit/EventKit.h>

@interface EventItem : NSObject
@property (nonatomic, readonly) EKEvent * event;
@property (nonatomic, readonly) NSString * title;
- (EventItem *)initWithEKEvent:(EKEvent *)event;

@end
