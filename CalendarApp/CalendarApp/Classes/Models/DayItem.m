//
//  CalendarItem.m
//  CalendarApp
//
//  Created by Pramod Jadhav on 08/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//

#import "DayItem.h"
#import "EventItem.h"

@implementation DayItem
@synthesize isSelected = _isSelected;
@synthesize haveEvents = _haveEvents;
@synthesize events = _events;

-(void)dealloc{
    
    _events = nil;
    _date = nil;
}

- (DayItem *)initWithDate:(NSDate *)date isSelected:(BOOL)selected{
    
    self = [super init];
    if (self) {
        
        _date = date;
        _isSelected = selected;
    }
    return self;
}

- (BOOL)haveEvents{
    
    [self events];
    return _haveEvents;
}

- (NSArray *)events{
    
    if (!_events) {
        
        if(!self.date){
            
            EventItem * eventItem = [[EventItem alloc] initWithEKEvent:nil];
            _events =  @[eventItem];
        }else{
            
            EKEventStore *store = [[EKEventStore alloc] init];
            // Create the predicate from the event store's instance method
            NSPredicate *predicate = [store predicateForEventsWithStartDate:[self.date da_beginningOfDay:nil]
                                                                    endDate:[self.date da_endOfDay:nil]
                                                                  calendars:nil];
            NSArray *events = [store eventsMatchingPredicate:predicate];
            NSMutableArray * array = [NSMutableArray new];
            
            if(![events count]){
                
                _haveEvents = NO;
                [array addObject:[[EventItem alloc] initWithEKEvent:nil]];
                
            }else{
                // Fetch all events that match the predicate
                for (EKEvent * event in events) {
                    EventItem * eventItem = [[EventItem alloc] initWithEKEvent:event];
                    [array addObject:eventItem];
                }
                _haveEvents = YES;
            }
            _events = [NSArray arrayWithArray:array];
            
        }
    }
    return _events;
}
@end
