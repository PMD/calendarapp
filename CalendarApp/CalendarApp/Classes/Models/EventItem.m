//
//  EventItem.m
//  CalendarApp
//
//  Created by Pramod Jadhav on 08/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//

#import "EventItem.h"

@implementation EventItem
@synthesize title = _title;

- (EventItem *)initWithEKEvent:(EKEvent *)event{
    
    self = [super init];
    if (self) {
        
        _event = event;
    }
    return self;

}

-(NSString *)title{
    
    _title = _event.title;
    if (!_title) {
        _title = @"No event";
    }
    return _title;
}
@end
