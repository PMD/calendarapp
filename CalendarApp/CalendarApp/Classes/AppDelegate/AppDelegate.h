//
//  AppDelegate.h
//  CalendarApp
//
//  Created by Pramod Jadhav on 07/05/16.
//  Copyright © 2016 Microsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

